# LNTXParser


## Installation

Make sure you have added the private **LINITIX** CocoaPods-Specs repository to your CocoaPods repository. To add it, run the following command in your terminal:

```
$ pod repo add LINITIX https://bitbucket.org/linitixios/cocoapods-specs.git
```

LNTXParser is available through [CocoaPods](http://cocoapods.org), to install
it simply add the following line to your Podfile:

```
pod "LNTXParser"
```

## Author

Damien Rambout, damien.rambout@linitix.com

*LINITIX All Rights Reserved.*


