# LNTXParser CHANGELOG

## 0.1.8

- **Update** (Improve) support for multi component node keys (`x/y/z`).

## 0.1.7

**TODO**

## 0.1.6

**TODO**

## 0.1.5

**TODO**

## 0.1.4

**TODO**

## 0.1.3

**TODO**

## 0.1.2

**TODO**

## 0.1.1

- **Removed** `LNTXStack` class. Now referenced from **LNTXCoreKit**.

## 0.1.0

Initial release.

- **Added** `LNTXParser`: A generic protocol for parsing data into data model objects.
- **Added** `LNTXJSONParser`: A JSON parser that can transform JSON data into data model objects.
- **Added** `LNTXParserConfiguration`: A class for creating parser configurations. Configuration can be loaded from plist files.