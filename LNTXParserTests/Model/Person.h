//
//  Person.h
//  LNTXParser
//
//  Created by Damien Rambout on 09/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic) NSInteger age;
@property (nonatomic, copy) NSArray *friends;
@property (nonatomic, readonly) NSArray *ids;
@property (nonatomic) NSArray *floats;
@property (nonatomic, copy) NSArray *meetingDates;
@property (nonatomic, copy) NSArray *classifiedFriends;

- (void)addId:(id)anId;

@end
