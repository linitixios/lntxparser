//
//  Person.m
//  LNTXParser
//
//  Created by Damien Rambout on 09/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "Person.h"

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>

@interface Person ()

@property (nonatomic) NSMutableArray *ids;

@end

@implementation Person

LNTXDefineAutoDescription();

- (id)init
{
    self = [super init];
    if (self) {
        self.ids = [NSMutableArray array];
    }
    return self;
}

- (void)addId:(id)anId {
    [(NSMutableArray *)self.ids addObject:anId];
}

@end
