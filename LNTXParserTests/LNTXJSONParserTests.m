//
//  LNTXJSONParserTests.m
//  LNTXParser
//
//  Created by Damien Rambout on 11/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LNTXParser.h"

#import "NSBundle+Tests.h"
#import "Person.h"

static NSDictionary *kPersonJSONObject;
static NSDictionary *kPersonWithFriendsJSONObject;
static NSString * kPersonJson;
static NSString * kPersonWithFriendsJson;

@interface NSDictionary (LNTXExtension)

- (instancetype)dictionaryByAddingEntriesFromDictionary:(NSDictionary *)dictionary;

@end

@implementation NSDictionary (LNTXExtension)

- (instancetype)dictionaryByAddingEntriesFromDictionary:(NSDictionary *)dictionary {
    NSMutableDictionary *mutableCopy = [self mutableCopy];
    [mutableCopy addEntriesFromDictionary:dictionary];
    return [mutableCopy copy];
}

@end

@interface LNTXJSONParserTests : XCTestCase

@property (nonatomic) LNTXParserConfiguration *personConfiguration;
@property (nonatomic) LNTXParserConfiguration *personArrayConfiguration;
@property (nonatomic) LNTXParserConfiguration *subPersonConfiguration;
@property (nonatomic) LNTXParserConfiguration *subPersonArrayConfiguration;

@end

@implementation LNTXJSONParserTests

+ (void)initialize {
//    @"{\"name\":{\"first_name\":\"Lenny\", \"last_name\":\"Ticks\"}, \"age\": 25}"
    kPersonJSONObject = @{@"name": @{@"first_name": @"Lenny",
                                     @"last_name": @"Ticks"},
                          @"age": @(25),
                          @"ids": @[@[@1, @5, @8, @12], @[@2, @6, @0, @13]],
                          @"floats": @[@[@1.0], @[@4.5, @6.8]],
                          @"meeting_dates": @[@"2013-02-14", @"2013-02-15", @"2013-02-16"],
                          @"classified_friends": @[@[@{@"name": @{@"first_name": @"Lenny",
                                                                  @"last_name": @"Ticks"}}],
                                                   @[@{@"name": @{@"first_name": @"Lenny",
                                                                  @"last_name": @"Ticks"}}]]};
    kPersonWithFriendsJSONObject = [kPersonJSONObject dictionaryByAddingEntriesFromDictionary:
                                    @{@"friends": @[kPersonJSONObject, kPersonJSONObject]}];
    
    kPersonJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:kPersonJSONObject
                                                                                 options:0
                                                                                   error:nil]
                                        encoding:NSUTF8StringEncoding];
    kPersonWithFriendsJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:kPersonWithFriendsJSONObject
                                                                                            options:0
                                                                                              error:nil]
                                                   encoding:NSUTF8StringEncoding];
}

+ (void)setUp {
    [NSBundle lntx_switchBundleMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
}

- (void)setUp
{
    [super setUp];
    
    self.personConfiguration = [LNTXParserConfiguration configurationFromPlistFileWithName:@"PersonWithFriends"
                                                                                  inBundle:nil
                                                                                     error:nil];
    self.personArrayConfiguration = [LNTXParserConfiguration configurationFromPlistFileWithName:@"PersonArray"
                                                                                       inBundle:nil
                                                                                          error:nil];
    self.subPersonArrayConfiguration = [LNTXParserConfiguration configurationFromPlistFileWithName:@"SubPersonArray"
                                                                                          inBundle:nil
                                                                                        error:nil];
    self.subPersonConfiguration = [LNTXParserConfiguration configurationFromPlistFileWithName:@"SubPerson"
                                                                                     inBundle:nil
                                                                                        error:nil];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testParserCanBeInitialized {
    LNTXJSONParser *parser = [[LNTXJSONParser alloc] initWithConfiguration:self.personConfiguration];
    XCTAssertNotNil(parser, @"Parser should not be nil when created with a configuration");
}

- (void)testParserThrowsWhenCreatedWithNoConfiguration {
    XCTAssertThrows([[LNTXJSONParser alloc] initWithConfiguration:nil],
                    @"Parser should throw when created with no configuration");
}

- (void)testParserCanParseStringObject {
    LNTXJSONParser *parser = [[LNTXJSONParser alloc] initWithConfiguration:self.personConfiguration];
    NSError *error;
    Person *person = [parser parseString:kPersonWithFriendsJson error:&error];
    
    NSLog(@"Person: %@", person);
    
    XCTAssertNotNil(person, @"Parser object should not be nil.");
}

- (void)testParserCanParseStringArray {
    NSString *json = [NSString stringWithFormat:@"[%@, %@, %@]", kPersonWithFriendsJson, kPersonWithFriendsJson, kPersonWithFriendsJson];
    
    LNTXJSONParser *parser = [[LNTXJSONParser alloc] initWithConfiguration:self.personArrayConfiguration];
    NSError *error;
    NSArray *persons = [parser parseString:json error:&error];
    
    NSLog(@"Persons: %@", persons);
    
    XCTAssertNotNil(persons, @"Parser array should not be nil.");
    XCTAssert([persons count] == 3, @"Array size should be 3.");
}

- (void)testParserCanParseStringSubObject {
    NSDictionary *jsonObject = @{@"data": kPersonJSONObject};
    NSString *json = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:jsonObject options:0 error:nil]
                                           encoding:NSUTF8StringEncoding];
    
    LNTXJSONParser *parser = [[LNTXJSONParser alloc] initWithConfiguration:self.subPersonConfiguration];
    NSError *error;
    id person = [parser parseString:json error:&error];
    
    NSLog(@"Person: %@", person);
    
    XCTAssertNotNil(person, @"Parser object should not be nil.");
}

- (void)testParserCanParseStringSubArray {
    NSString *json = [NSString stringWithFormat:@"{\"data\": [%@, %@, %@]}", kPersonWithFriendsJson, kPersonWithFriendsJson, kPersonWithFriendsJson];
    
    LNTXJSONParser *parser = [[LNTXJSONParser alloc] initWithConfiguration:self.subPersonArrayConfiguration];
    NSError *error;
    NSArray *persons = [parser parseString:json error:&error];
    
    NSLog(@"Persons: %@", persons);
    
    XCTAssertNotNil(persons, @"Parser array should not be nil.");
    XCTAssert([persons count] == 3, @"Array size should be 3.");
}

@end
