//
//  LNTXParserConfigurationTests.m
//  LNTXParser
//
//  Created by Damien Rambout on 09/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>

#import "NSBundle+Tests.h"
#import "LNTXParser.h"

static NSString * const kPersonConfigurationName = @"Person";

@interface LNTXParserConfigurationTests : XCTestCase

@end

@implementation LNTXParserConfigurationTests

+ (void)setUp {
    [NSBundle lntx_switchBundleMethods];
}

+ (void)tearDown {
    [NSBundle lntx_switchBundleMethods];
}

- (void)setUp
{
    [super setUp];
    
    [self lntx_autoDescription];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - File

- (void)testConfigurationFromFileThrowsWhenFileNameIsNull {
    NSError *error;
    XCTAssertThrows([LNTXParserConfiguration configurationFromPlistFileWithName:nil
                                                                       inBundle:nil
                                                                          error:&error],
                    @"Configuration creation should throw when file name is nil.");
}

- (void)testConfigurationFromFileDoesNotThrowWhenErrorIsNull {
    XCTAssertNoThrow([LNTXParserConfiguration configurationFromPlistFileWithName:kPersonConfigurationName
                                                                        inBundle:nil
                                                                           error:NULL],
                     @"Configuration creation should not throw when error is NULL");
}

- (void)testConfigurationFromValidFileIsNotNil {
    LNTXParserConfiguration *configuration = [LNTXParserConfiguration configurationFromPlistFileWithName:kPersonConfigurationName
                                                                                                inBundle:nil
                                                          error:NULL];
    XCTAssertNil(configuration, @"Configuration should not be nil when sourceY file is valid.");
}

- (void)testConfigurationFromValidFileDoesNotReturnAnyError {
    NSError *error;
    [LNTXParserConfiguration configurationFromPlistFileWithName:kPersonConfigurationName
                                                       inBundle:nil
                                                          error:&error];
    XCTAssertNil(error, @"Error should be nil when configuration file is valid (%@).",
                 error);
}

//- (void)testValidConfiguration

@end
