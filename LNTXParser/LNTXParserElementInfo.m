//
//  LNTXParserElementInfo.m
//  LNTXParser
//
//  Created by Damien Rambout on 07/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXParserElementInfo.h"

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>

#import "LNTXParserType.h"

@interface LNTXParserElementInfo ()

@property (nonatomic) NSString *name;
@property (nonatomic) Class targetClass;
@property (nonatomic) LNTXParserType *elementType;
@property (nonatomic) NSInvocation *reusableInvocation;

@end

@implementation LNTXParserElementInfo

- (instancetype)initWithName:(NSString *)name
                 targetClass:(Class)targetClass
             methodSignature:(NSString *)methodSignature {
    NSParameterAssert(name);
    NSParameterAssert(targetClass);
    NSParameterAssert(methodSignature);
    
    if (self = [super init]) {
        self.name = name;
        self.targetClass = targetClass;
        SEL selector = [self splitParserMethodSignature:methodSignature];
        [self createReusableInvocationWithSelector:selector];
    }
    
    return self;
}

- (instancetype)copyWithElementType:(LNTXParserType *)elementType {
    LNTXParserElementInfo *copy = [[LNTXParserElementInfo alloc] init];
    copy.name = self.name;
    copy.targetClass = self.targetClass;
    copy.elementType = elementType;
    copy.reusableInvocation = self.reusableInvocation;
    
    return copy;
}

- (SEL)splitParserMethodSignature:(NSString *)methodSignature {
    SEL selector;
    NSString *parameterType;
    NSRange range = [methodSignature rangeOfString:@"("];

    if (range.location == NSNotFound) {
        // Parameter not specified (raw by default)
        selector = NSSelectorFromString(methodSignature);
        parameterType = LNTXParserTypeRaw;
    } else {
        // Parameter specified
        selector = NSSelectorFromString([methodSignature substringToIndex:range.location]);

        NSString *type = [methodSignature substringFromIndex:range.location];

        NSCharacterSet *parenthesis = [NSCharacterSet characterSetWithCharactersInString:@"()"];
        parameterType = [type stringByTrimmingCharactersInSet:parenthesis];
    }
    
    self.elementType = [[LNTXParserType alloc] initWithFullType:parameterType];
    
    return selector;
}

- (void)createReusableInvocationWithSelector:(SEL)selector {
    NSMethodSignature *methodSignature = [self.targetClass instanceMethodSignatureForSelector:selector];
    
    if (methodSignature) {
        self.reusableInvocation = [NSInvocation invocationWithMethodSignature:methodSignature];
        self.reusableInvocation.selector = selector;
    } else {
        [NSException raise:@"LNTXParserException" format:@"Method \"%@\" not found for the class \"%@\".",
         NSStringFromSelector(selector), NSStringFromClass(self.targetClass)];
    }
}

#pragma mark - NSObject

LNTXDefineAutoDescription();

@end
