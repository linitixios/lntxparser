//
//  LNTXJSONContext.h
//  LNTXParser
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const LNTXJSONArrayNodeName;

@protocol LNTXParserConfiguration;

@interface LNTXJSONContext : NSObject

+ (instancetype)contextWithConfiguration:(id <LNTXParserConfiguration>)configuration;

@property (nonatomic, readonly) id createdObject;
@property (nonatomic, readonly) NSString *nodeName;
@property (nonatomic, readonly) LNTXJSONContext *parentContext;

- (BOOL)canHandleNodeWithName:(NSString *)nodeName;

- (BOOL)isPrimitiveNode:(NSString *)nodeName;
- (BOOL)isContainerNode:(NSString *)nodeName;

- (void)setPrimitiveValue:(id)value forNodeWithName:(NSString *)nodeName;
- (void)setObjectValue:(id)value forNodeWithName:(NSString *)nodeName;

- (LNTXJSONContext *)contextForObjectNodeWithName:(NSString *)nodeName;
- (LNTXJSONContext *)contextForSkippedNodeWithName:(NSString *)nodeName;

@end
