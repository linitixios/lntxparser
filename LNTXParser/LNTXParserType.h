//
//  LNTXParserType.h
//  LNTXParser
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const LNTXParserTypeRaw;
extern NSString * const LNTXParserTypeArray;
extern NSString * const LNTXParserTypeString;
extern NSString * const LNTXParserTypeChar;
extern NSString * const LNTXParserTypeInteger;
extern NSString * const LNTXParserTypeLong;
extern NSString * const LNTXParserTypeFloat;
extern NSString * const LNTXParserTypeDouble;
extern NSString * const LNTXParserTypeBoolean;
extern NSString * const LNTXParserTypeDate;
extern NSString * const LNTXParserTypeTimestamp;
extern NSString * const LNTXParserTypeURL;

@interface LNTXParserType : NSObject

+ (Class)classForType:(NSString *)type;

+ (BOOL)isPrimitiveType:(NSString *)type;
+ (BOOL)isObjectType:(NSString *)type;
+ (BOOL)isArrayType:(NSString *)type;

@property (nonatomic, readonly) NSString *mainType;
@property (nonatomic, readonly) LNTXParserType *subType;

- (instancetype)initWithFullType:(NSString *)fullType;
- (instancetype)initWithMainType:(NSString *)mainType
                         subType:(LNTXParserType *)subType;

- (id)instantiate;

- (BOOL)isPrimitive;
- (BOOL)isObject;
- (BOOL)isArray;

@end
