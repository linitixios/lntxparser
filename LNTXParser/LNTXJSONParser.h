//
//  LNTXJSONParser.h
//  LNTXParser
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <LNTXParser/LNTXParser.h>

@interface LNTXJSONParser : NSObject
<LNTXParser>

@end
