//
//  LNTXParserElementInfo.h
//  LNTXParser
//
//  Created by Damien Rambout on 07/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LNTXParserType;

@protocol LNTXParserElementInfo <NSObject>

/** The name of the element. */
@property (nonatomic, readonly) NSString *name;
/** Class of the model object to which assign data. */
@property (nonatomic, readonly) Class targetClass;
/** The type of parameter that should be passed to update model data. */
@property (nonatomic, readonly) LNTXParserType *elementType;
/** A reusable NSInvocation object that is already prepared to update model data. */
@property (nonatomic, readonly) NSInvocation *reusableInvocation;

@end

@interface LNTXParserElementInfo : NSObject
<LNTXParserElementInfo>

- (instancetype)initWithName:(NSString *)name
                 targetClass:(Class)targetClass
             methodSignature:(NSString *)methodSignature;

- (instancetype)copyWithElementType:(LNTXParserType *)elementType;

@end
