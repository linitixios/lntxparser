//
//  LNTXParserType.m
//  LNTXParser
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXParserType.h"

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>

NSString * const LNTXParserTypeRaw = @"raw";
NSString * const LNTXParserTypeArray = @"array";
NSString * const LNTXParserTypeString = @"string";
NSString * const LNTXParserTypeChar = @"char";
NSString * const LNTXParserTypeInteger = @"int";
NSString * const LNTXParserTypeLong = @"long";
NSString * const LNTXParserTypeFloat = @"float";
NSString * const LNTXParserTypeDouble = @"double";
NSString * const LNTXParserTypeBoolean = @"bool";
NSString * const LNTXParserTypeDate = @"date";
NSString * const LNTXParserTypeTimestamp = @"timestamp";
NSString * const LNTXParserTypeURL = @"url";

@interface LNTXParserType ()

@property (nonatomic) NSString *mainType;
@property (nonatomic) LNTXParserType *subType;

@end

static NSArray *kPrimitiveTypes;

@implementation LNTXParserType

+ (void)initialize {
    kPrimitiveTypes = @[LNTXParserTypeRaw,
                        LNTXParserTypeString,
                        LNTXParserTypeInteger,
                        LNTXParserTypeLong,
                        LNTXParserTypeFloat,
                        LNTXParserTypeDouble,
                        LNTXParserTypeBoolean,
                        LNTXParserTypeDate,
                        LNTXParserTypeTimestamp,
                        LNTXParserTypeURL,
                        LNTXParserTypeChar];
}

+ (Class)classForType:(NSString *)type {
    if ([type isEqualToString:LNTXParserTypeArray]) {
        return [NSMutableArray class];
    }
    
    return NSClassFromString(type);
}

+ (BOOL)isPrimitiveType:(NSString *)type {
    return [kPrimitiveTypes containsObject:type];
}

+ (BOOL)isObjectType:(NSString *)type {
    return ![self isPrimitiveType:type];
}

+ (BOOL)isArrayType:(NSString *)type {
    return [type isEqualToString:LNTXParserTypeArray];
}

- (instancetype)initWithFullType:(NSString *)fullType {
    NSString *tmpMainType = fullType;
    NSString *tmpSubType = nil;
    LNTXParserType *subType = nil;
    
    // TODO: Improve type parsing
    NSRange subTypeOpenCharRange = [tmpMainType rangeOfString:@"<"];
    NSRange subTypeCloseCharRange = [tmpMainType rangeOfString:@">" options:NSBackwardsSearch];
    
    if (subTypeOpenCharRange.location != NSNotFound && subTypeCloseCharRange.location != NSNotFound) {
        // Found a subtype
        tmpMainType = [fullType substringToIndex:subTypeOpenCharRange.location];
        tmpSubType = [fullType substringWithRange:NSMakeRange(subTypeOpenCharRange.location + 1,
                                                              subTypeCloseCharRange.location -
                                                              (subTypeOpenCharRange.location + 1))];
        
        if (tmpSubType.length == 0) {
            tmpSubType = LNTXParserTypeRaw;
        }
        
        subType = [[LNTXParserType alloc] initWithFullType:tmpSubType];
    }
    
    return [self initWithMainType:tmpMainType
                          subType:subType];
}

- (instancetype)initWithMainType:(NSString *)mainType
                         subType:(LNTXParserType *)subType {
    if (mainType.length == 0) {
        return nil;
    }
    
    if (self = [super init]) {
        self.mainType = mainType;
        self.subType = subType;
    }
    
    return self;
}

- (id)instantiate {
    Class class = [self.class classForType:self.mainType];
    
    return [[class alloc] init];
}

- (BOOL)isPrimitive {
    return [self.class isPrimitiveType:self.mainType];
}

- (BOOL)isObject {
    return [self.class isObjectType:self.mainType];
}

- (BOOL)isArray {
    return [self.class isArrayType:self.mainType];
}

#pragma mark - NSObject

LNTXDefineAutoDescription();

@end
