//
//  LNTXJSONParser.m
//  LNTXParser
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXJSONParser.h"

#import <LNTXCoreKit/LNTXStack.h>
#import "LNTXJSONContext.h"
#import "LNTXParserConfiguration.h"

@interface LNTXJSONParser ()

@property (nonatomic) id<LNTXParserConfiguration> configuration;
@property (nonatomic) LNTXStack *contexts;

@end

@implementation LNTXJSONParser

- (instancetype)initWithConfiguration:(id<LNTXParserConfiguration>)configuration {
    if (self = [super init]) {
        if (!configuration) {
            [NSException raise:@"LNTXParserException" format:@"The configuration of a parser must not be nil."];
            return nil;
        }
        
        self.configuration = configuration;
    }
    
    return self;
}

- (id)parserData:(id)data error:(NSError **)error {
    return [self parserData:data usingEncoding:NSUTF8StringEncoding error:error];
}

- (id)parserData:(id)data usingEncoding:(NSStringEncoding)encoding error:(NSError **)error {
    if (!data) {
        [NSException raise:@"LNTXParserException" format:@"The JSON data to parse must not be nil."];
        return nil;
    }
    
    id object = [NSJSONSerialization JSONObjectWithData:data
                                                options:0
                                                  error:error];
    if (!object) {
        return nil;
    }
    
    return [self parseObject:object error:error];
}

- (id)parseString:(NSString *)string error:(NSError **)error {
    if (!string) {
        [NSException raise:@"LNTXParserException" format:@"The JSON string to parse must not be nil."];
    }
    
    return [self parserData:[string dataUsingEncoding:NSUTF8StringEncoding]
              usingEncoding:NSUTF8StringEncoding
                      error:error];
}

- (id)parseObject:(id)object error:(NSError **)error {
    if (!object) {
        [NSException raise:@"LNTXParserException" format:@"The JSON object to parse must not be nil."];
    }
    
    self.contexts = [LNTXStack stack];
    
    // Initial context
    LNTXJSONContext *initialContext = [LNTXJSONContext contextWithConfiguration:self.configuration];
    [self.contexts pushObject:initialContext];
    [self handleContainerValue:object];
    [self.contexts popObject];
    
    return initialContext.createdObject;
}

#pragma mark - Parsing

- (void)handleValue:(id)value forElement:(NSString *)elementName {
    LNTXJSONContext *currentContext = [self.contexts topObject];
    
    if ([currentContext canHandleNodeWithName:elementName]) {
        if ([currentContext isContainerNode:elementName]) {
            // Container (object or array)
            LNTXJSONContext *newContext = [currentContext contextForObjectNodeWithName:elementName];
//            newContext.verbose = _verbose;
            [self.contexts pushObject:newContext];
            [self handleContainerValue:value];
            [self.contexts popObject];
            
            [currentContext setObjectValue:newContext.createdObject forNodeWithName:elementName];
        } else {
            // Primitive
            if (![[NSNull null] isEqual:value] && value != nil)
            {
                [currentContext setPrimitiveValue:value forNodeWithName:elementName];
            }
        }
    } else if ([self isContainerValue:value]) {
        // Skipped (seek deeper)
        LNTXJSONContext *skippedContext = [currentContext contextForSkippedNodeWithName:elementName];
//        skippedContext.verbose = _verbose;
        [self.contexts pushObject:skippedContext];
        [self handleContainerValue:value];
        [self.contexts popObject];
    }
}

- (void)handleContainerValue:(id)value {
    if ([value isKindOfClass:[NSArray class]]) {
        // Array
        [self handleArray:value];
    } else if ([value isKindOfClass:[NSDictionary class]]) {
        // Dictionary
        [self handleDictionary:value];
    }
}

- (BOOL)isContainerValue:(id)value {
    return ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]]);
}

- (void)handleDictionary:(NSDictionary *)dictionary {
    [dictionary enumerateKeysAndObjectsUsingBlock:^(id elementName, id value, BOOL *stop) {
        [self handleValue:value forElement:elementName];
    }];
}

- (void)handleArray:(NSArray *)array {    
    for (id value in array) {
        // Default element name provided for array elements
        [self handleValue:value forElement:LNTXJSONArrayNodeName];
    }
}

@end
