//
//  LNTXParserConfiguration.m
//  LNTXParser
//
//  Created by Damien Rambout on 07/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXParserConfiguration.h"

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>
#import <LNTXCoreKit/LNTXSingleton.h>

#import "LNTXParserType.h"
#import "LNTXParserElementInfo.h"

@interface LNTXParserConfiguration ()

@property (nonatomic) NSBundle *bundle;
@property (nonatomic) LNTXParserType *targetType;
@property (nonatomic) NSMutableDictionary *nodeMappings;
@property (nonatomic) NSString *prefix;

@end

@interface LNTXParserConfigurationFactory : NSObject

+ (instancetype)sharedFactory;

@property (nonatomic) NSMutableDictionary *cachedConfigurations;

- (LNTXParserConfiguration *)configurationForKey:(NSString *)key inBundle:(NSBundle *)bundle;
- (void)setConfiguration:(LNTXParserConfiguration *)configuration forKey:(NSString *)key;

@end

@implementation LNTXParserConfiguration

#pragma mark - NSObject

LNTXDefineAutoDescription();

#pragma mark - Initializers

+ (instancetype)configurationFromPlistFileWithName:(NSString *)fileName
                                             error:(NSError **)error {
    return [self configurationFromPlistFileWithName:fileName
                                           inBundle:nil
                                              error:error];
}

+ (instancetype)configurationFromPlistFileWithName:(NSString *)fileName
                                          inBundle:(NSBundle *)bundle
                                             error:(NSError **)error {
    if (!fileName) {
        return nil;
    }
    
    NSBundle *fileBundle = bundle ?: [NSBundle mainBundle];
    
    LNTXParserConfiguration *configuration = [[LNTXParserConfigurationFactory sharedFactory] configurationForKey:fileName
                                                                                                        inBundle:fileBundle];
    
    if (!configuration) {
        NSString *filePath = [fileBundle pathForResource:fileName ofType:@"plist"];
        
        NSDictionary *fileContent = [NSDictionary dictionaryWithContentsOfFile:filePath];
        if (!fileContent) {
            if (error) {
                // Invalid file
                *error = [NSError errorWithDomain:@"LNTXParserErrorDomain" code:1 userInfo:nil];
            }
            
            return nil;
        }
        
        // Target class
        id superConfiguration = fileContent[@"super"];
        NSString *targetClassName = fileContent[@"class"];
        
        if (!targetClassName && !superConfiguration) {
            // Throw exception because no classe could be defined
            [NSException raise:@"LNTXNoTargetClassDefinedException"
                        format:@"The configuration '%@' is missing a target class.", fileName];
        }
        
        if (!targetClassName && [superConfiguration isKindOfClass:[NSArray class]] && [superConfiguration count] > 1) {
            // Throw exception because no class defined and multiple super configurations
            [NSException raise:@"LNTXNoTargetClassDefinedException"
                        format:@"The configuration '%@' is missing a target class.", fileName];
        }
        
        if (targetClassName) {
            configuration = [self configurationWithTargetClassName:targetClassName inBundle:bundle error:error];
            if (!configuration) {
                return nil;
            }
        } else {
            configuration = [[LNTXParserConfiguration alloc] init];
        }
        
        configuration.bundle = fileBundle;
        
        [[LNTXParserConfigurationFactory sharedFactory] setConfiguration:configuration forKey:fileName];
        
        // Super configuration
        if (superConfiguration) {
            if ([superConfiguration isKindOfClass:[NSArray class]]) {
                [configuration addContentFromConfigurationsWithNames:superConfiguration];
            } else {
                [configuration addContentFromConfigurationWithName:superConfiguration];
            }
        }
        
        // Prefix
        configuration.prefix = fileContent[@"prefix"];
        
        // Nodes
        NSDictionary *rawNodeMappings = fileContent[@"nodes"];
        
        if (rawNodeMappings) {
            [configuration.nodeMappings addEntriesFromDictionary:[configuration elementMappingsWithDictionary:rawNodeMappings]];
        }
    }
    
    // Always return a copy
    return [configuration copy];
}

+ (instancetype)configurationWithTargetClassName:(NSString *)className error:(NSError **)error {
    return [self configurationWithTargetClassName:className inBundle:nil error:error];
}

+ (instancetype)configurationWithTargetClassName:(NSString *)className inBundle:(NSBundle *)bundle error:(NSError **)error {
    if (className.length == 0) {
        if (error) {
            // Class not provided
            *error = [NSError errorWithDomain:@"LNTXParserErrorDomain" code:2 userInfo:nil];
        }
        
        return nil;
    }
    
    LNTXParserType *targetType = [[LNTXParserType alloc] initWithFullType:className];
    
    if (!targetType) {
        if (error) {
            // Class invalid or empty
            *error = [NSError errorWithDomain:@"LNTXParserErrorDomain" code:2 userInfo:nil];
        }
        
        return nil;
    }
    
    if ([targetType isObject]) {
        // Flatten type (i.e. inline arrays)
        targetType = [self typeByFlatenningType:targetType inBundle:bundle shouldFlattenMainType:NO];
    }
    
    LNTXParserConfiguration *configuration = [[LNTXParserConfiguration alloc] init];
    configuration.bundle = bundle;
    configuration.targetType = targetType;
    
    return configuration;
}

- (id)init
{
    if (self = [super init]) {
        self.nodeMappings = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    LNTXParserConfiguration *copy = [[LNTXParserConfiguration alloc] init];
    
    copy.bundle = self.bundle;
    copy.targetType = self.targetType;
    copy.prefix = self.prefix;
    copy.nodeMappings = [self.nodeMappings mutableCopy];
    
    return copy;
}

#pragma mark - Super configuration

- (void)addContentFromConfigurationWithName:(NSString *)configurationName {
    // TODO: Handle error
    LNTXParserConfiguration *configuration = [LNTXParserConfiguration configurationFromPlistFileWithName:configurationName
                                                                                                inBundle:self.bundle
                                                                                                   error:nil];
    if (configuration) {
        // Add all nodes
        [self.nodeMappings addEntriesFromDictionary:configuration.nodeMappings];
        if (!self.targetType) {
            self.targetType = configuration.targetType;
        }
    }
}

- (void)addContentFromConfigurationsWithNames:(NSArray *)configurationNames {
    for (NSString *configurationName in configurationNames) {
        [self addContentFromConfigurationWithName:configurationName];
    }
}

#pragma mark - Accessing nodes

- (NSArray *)allNodes {
    return [self.nodeMappings allValues];
}

- (id<LNTXParserElementInfo>)nodeForName:(NSString *)nodeName {
    return self.nodeMappings[nodeName];
}

#pragma mark - Updating nodes

- (id<LNTXParserElementInfo>)addNodeWithName:(NSString *)nodeName
                                    selector:(SEL)selector
                               parameterType:(NSString *)parameterType {
    NSParameterAssert(nodeName);
    NSParameterAssert(parameterType);
    
    NSString *methodSignature = [NSString stringWithFormat:@"%@(%@)", NSStringFromSelector(selector), parameterType];
    
    LNTXParserElementInfo *info = [[LNTXParserElementInfo alloc] initWithName:nodeName
                                                                  targetClass:[LNTXParserType classForType:self.targetType.mainType]
                                                              methodSignature:methodSignature];
    
    self.nodeMappings[nodeName] = info;
    
    return info;
}

- (BOOL)removeNodeWithName:(NSString *)nodeName {
    if ([self.nodeMappings objectForKey:nodeName]) {
        [self.nodeMappings removeObjectForKey:nodeName];
        return YES;
    }

    return NO;
}

#pragma mark - Helper methods

- (NSDictionary *)elementMappingsWithDictionary:(NSDictionary *)dictionary {
    NSMutableDictionary *mappings = [NSMutableDictionary dictionaryWithCapacity:[dictionary count]];
    
    Class targetClass = [LNTXParserType classForType:self.targetType.mainType];
    NSString *prefix = self.prefix;
    
    [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString *name,
                                                    NSString *methodSignature,
                                                    BOOL *stop) {
        NSString *fullName = name;
        if (prefix.length > 0) {
            // Add prefix
            fullName = [NSString stringWithFormat:@"%@/%@", prefix, name];
        }
        
        LNTXParserElementInfo *elementInfo = [[LNTXParserElementInfo alloc] initWithName:fullName
                                                                             targetClass:targetClass
                                                                         methodSignature:methodSignature];
        
        if ([[elementInfo elementType] isObject]) {
            // Flatten type (i.e. inline arrays)
            LNTXParserType *flattenType = [self.class typeByFlatenningType:[elementInfo elementType]
                                                                  inBundle:self.bundle
                                                     shouldFlattenMainType:YES];
            elementInfo = [elementInfo copyWithElementType:flattenType];
        }
        
        mappings[fullName] = elementInfo;
    }];
    
    return [mappings copy];
}

+ (LNTXParserType *)typeByFlatenningType:(LNTXParserType *)parserType
                                inBundle:(NSBundle *)bundle
                   shouldFlattenMainType:(BOOL)shouldFlattenMainType {
    if (shouldFlattenMainType && [parserType isObject] && ![parserType isArray]) {
        LNTXParserConfiguration *elementConfiguration = [LNTXParserConfiguration configurationFromPlistFileWithName:parserType.mainType
                                                                                                           inBundle:bundle
                                                                                                              error:nil];
        
        if (elementConfiguration) {
            if ([elementConfiguration.targetType.mainType isEqualToString:LNTXParserTypeArray]) {
                // Replace with inline array
                return elementConfiguration.targetType;
            }
        }
    }
    
    LNTXParserType *flattenSubType = nil;
    if ([parserType isArray]) {
        flattenSubType = [self typeByFlatenningType:parserType.subType
                                           inBundle:bundle
                              shouldFlattenMainType:YES];
    } else {
        flattenSubType = parserType.subType;
    }
    
    return [[LNTXParserType alloc] initWithMainType:parserType.mainType
                                            subType:flattenSubType];
}

@end

@implementation LNTXParserConfigurationFactory

LNTXSingleton(sharedFactory);

- (id)init
{
    self = [super init];
    if (self) {
        self.cachedConfigurations = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (LNTXParserConfiguration *)configurationForKey:(NSString *)key inBundle:(NSBundle *)bundle {
    NSParameterAssert(bundle);
    NSParameterAssert(key);
    return self.cachedConfigurations[[self fullKeyWithBase:key bundle:bundle]];
}

- (void)setConfiguration:(LNTXParserConfiguration *)configuration forKey:(NSString *)key {
    NSParameterAssert(configuration);
    NSParameterAssert(key);
    self.cachedConfigurations[[self fullKeyWithBase:key bundle:configuration.bundle]] = configuration;
}

- (NSString *)fullKeyWithBase:(NSString *)base bundle:(NSBundle *)bundle {
    NSParameterAssert(base);
    
    if (bundle.bundleIdentifier != nil) {
        return [NSString stringWithFormat:@"%@.%@", bundle.bundleIdentifier, base];
    }
    
    return base;
}

@end
