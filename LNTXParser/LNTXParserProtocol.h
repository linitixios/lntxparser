//
//  LNTXParser.h
//  LNTXParser
//
//  Created by Damien Rambout on 12/03/15.
//  Copyright (c) 2015 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LNTXParserConfiguration;

/**
 @brief The LNTXParser protocol groups methods that are fundamental to all OR-Mapping based parsers.
 
 If an object conforms to this protocol, it can be considered a OR-Mapping based parser. Such an object can do the
 following tasks:
 
 - Parse a valid NSString object into a data model object
 - Parse a valid parser-ready object into a data model object
 - Parse a valid NSData object into a data model object
 */
@protocol LNTXParser <NSObject>

/** The configuration used to parse data. */
@property (nonatomic, readonly) id<LNTXParserConfiguration> configuration;

/**
 Returns an initialized id<LNTXParser> with a configuration that will be used to parse data.
 @param configuration The configuration used to parse data. This parameter must not be nil.
 @return An initialized id<LNTXParser> with a configuration that will be used to parse data.
 */
- (instancetype)initWithConfiguration:(id<LNTXParserConfiguration>)configuration;

/**
 @brief Parses a string and convert it into a data model object using the parser's configuration.
 @param string The string to parse.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @return An object obtained by parsing the string parameter. If the string can’t be parsed or there is an encoding
 error, returns nil.
 */
- (id)parseString:(NSString *)string error:(NSError **)error;

/**
 @brief Parses an parser-ready object and convert it into a data model object using the parser's configuration.
 @param object The object to parse.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @return An object obtained by parsing the object parameter. If the object can’t be parsed, used or there is an encoding
 error, returns nil.
 */
- (id)parseObject:(id)object error:(NSError **)error;

/**
 @brief Parses data and convert it into a data model object using the parser's configuration. Data must contain an UTF-8
 encoded string.
 @param data The data to parse.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @return An object obtained by parsing the data parameter. If the data can’t be parsed, used or there is an encoding
 error, returns nil.
 */
- (id)parserData:(id)data error:(NSError **)error;

/**
 @brief Parses data and convert it into a data model object using the parser's configuration. Data must contain a string
 encoded using the specified encoding.
 @param data The data to parse.
 @param encoding The encoding used to store the string in the data object.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @return An object obtained by parsing the data parameter. If the object can’t be parsed, used or there is an encoding
 error, returns nil.
 */
- (id)parserData:(id)data usingEncoding:(NSStringEncoding)encoding error:(NSError **)error;

@end
