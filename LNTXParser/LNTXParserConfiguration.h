//
//  LNTXParserConfiguration.h
//  LNTXParser
//
//  Created by Damien Rambout on 07/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LNTXParserElementInfo;
@class LNTXParserType;

@protocol LNTXParserConfiguration <NSObject, NSCopying>

#pragma mark - Initializers

/**
 @brief Returns a configuration filled with information retrieved from a file with a specific name.
 @param fileName The name of the configuration. This parameter must not be nil.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @return A prefilled configuration from a local file. If the file can’t be read or there is a format
 error, returns nil.
 */
+ (instancetype)configurationFromPlistFileWithName:(NSString *)fileName
                                             error:(NSError **)error;

/**
 @brief Returns a configuration filled with information retrieved from a file with a specific name.
 @param fileName The name of the configuration. This parameter must not be nil.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @param bundle The bundle from which the plist file must be loaded.
 @return A prefilled configuration from a local file. If the file can’t be read or there is a format
 error, returns nil.
 */
+ (instancetype)configurationFromPlistFileWithName:(NSString *)fileName
                                          inBundle:(NSBundle *)bundle
                                             error:(NSError **)error;

/**
 @brief Returns a configuration with a specific root class. The class is inferred from the name specified. Note that the
 name of this class must respect the format used for the parser that will use this configuration.
 @param class The name of a class to use as root for this configuration. This parameter must not be nil.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @return A a configuration with a specific root class. If the class can't be inferred, returns nil.
 */
+ (instancetype)configurationWithTargetClassName:(NSString *)className error:(NSError **)error;

+ (instancetype)configurationWithTargetClassName:(NSString *)className inBundle:(NSBundle *)bundle error:(NSError **)error;

/**
 @brief Returns a configuration with a specific root class.
 @param class A class to use as root for this configuration. This parameter must not be nil.
 @param error If an error occurs, upon returns contains an NSError object that describes the problem. If you are not
 interested in possible errors, pass in NULL.
 @return A a configuration with a specific root class.
 */
//+ (instancetype)configurationForClass:(Class)aClass;

#pragma mark - Accessors

@property (nonatomic, readonly) NSBundle *bundle;
@property (nonatomic, readonly) LNTXParserType *targetType;
@property (nonatomic, readonly) NSString *prefix;

#pragma mark - Accessing nodes

/**
 @brief Returns all nodes contained in this configuration. This consists in an array of id<LNTXParserElementInfo> objects.
 @return All nodes contained in this configuration.
 */
- (NSArray *)allNodes;

/**
 @brief Returns information about a node that has the specified name. If no node can be found for this name, then nil is
 returned.
 @param The name of the node to find.
 @return Information about a node that has the specified name, or nil if none is found.
 */
- (id <LNTXParserElementInfo>)nodeForName:(NSString *)nodeName;

#pragma mark - Updating nodes

/**
 @brief Adds a node with a specific name and maps a selector and parameter information to it. If a node already exists with
 this name, new information will override the existing one.
 @param nodeName The name of the node to add.
 @param selector The selector of the method to call to assign data for this node.
 @return The node created with the information provided. If information are invalid, nil is returned.
 */
- (id <LNTXParserElementInfo>)addNodeWithName:(NSString *)nodeName selector:(SEL)selector parameterType:(NSString *)parameterType;

/**
 @brief Removes a node with a specific name. If no node exists for this name, nothing happens.
 @return YES if a node was removed, NO otherwise.
 */
- (BOOL)removeNodeWithName:(NSString *)nodeName;

@end

@interface LNTXParserConfiguration : NSObject
<LNTXParserConfiguration>

@end
