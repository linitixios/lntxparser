//
//  LNTXJSONContext.m
//  LNTXParser
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXJSONContext.h"

#import "LNTXParserConfiguration.h"
#import "LNTXParserElementInfo.h"
#import "LNTXParserType.h"

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>

NSString * const LNTXJSONArrayNodeName = @"*";

@interface LNTXJSONContext ()

@property (nonatomic) id <LNTXParserConfiguration>configuration;
@property (nonatomic) id createdObject;
@property (nonatomic) NSString *nodeName;
@property (nonatomic) LNTXJSONContext *parentContext;
@property (nonatomic) NSBundle *bundle;

@end

@interface LNTXJSONArrayContext : LNTXJSONContext

@property (nonatomic) LNTXParserType *contentType;
@property (nonatomic) NSString *acceptedNodeName;

@end

@interface LNTXJSONSkippedContext : LNTXJSONContext

@end

@implementation LNTXJSONContext

+ (instancetype)contextWithConfiguration:(id <LNTXParserConfiguration>)configuration {
    if ([configuration.targetType.mainType isEqualToString:LNTXParserTypeArray]) {
        // Array configuration
        return [[LNTXJSONArrayContext alloc] initWithConfiguration:configuration];
    }
    
    // Regular configuration
    return [[LNTXJSONContext alloc] initWithConfiguration:configuration];
}

- (instancetype)initWithConfiguration:(id <LNTXParserConfiguration>)configuration {
    if (self = [super init]) {
        self.configuration = configuration;
        self.createdObject = [configuration.targetType instantiate];
        self.bundle = [configuration bundle];
    }
    
    return self;
}

- (instancetype)initWithNodeName:(NSString *)nodeName parentContext:(LNTXJSONContext *)parentContext {
    LNTXParserType *nodeType = [parentContext typeForNodeWithName:nodeName];
    // TODO: Handle error
    if ([nodeType.mainType isEqualToString:LNTXParserTypeArray]) {
        // Array
        if (self = [super init]) {
            self.createdObject = [nodeType instantiate];
        }
    } else {
        // Regular configuration
        id <LNTXParserConfiguration>configuration = [LNTXParserConfiguration configurationFromPlistFileWithName:nodeType.mainType
                                                                                                       inBundle:[parentContext bundle]
                                                                                                          error:nil];
        
        self = [self initWithConfiguration:configuration];
    }
    
    if (self) {
        self.nodeName = nodeName;
        self.parentContext = parentContext;
        self.bundle = parentContext.bundle;
    }
    
    return self;
}

LNTXDefineAutoDescription();

- (LNTXJSONContext *)contextForObjectNodeWithName:(NSString *)nodeName {
    LNTXParserType *nodeType = [self typeForNodeWithName:nodeName];
    
    if ([nodeType.mainType isEqualToString:LNTXParserTypeArray]) {
        // Array
        return [[LNTXJSONArrayContext alloc] initWithNodeName:nodeName parentContext:self];
    }
    
    // Regular configuration
    return [[LNTXJSONContext alloc] initWithNodeName:nodeName parentContext:self];
}

- (LNTXJSONContext *)contextForSkippedNodeWithName:(NSString *)nodeName {
    return [[LNTXJSONSkippedContext alloc] initWithNodeName:nodeName parentContext:self];
}

- (BOOL)canHandleNodeWithName:(NSString *)nodeName {
    return [self elementInfoForNodeWithName:nodeName] != nil || [self.parentContext canHandleNodeWithName:[self nodeNameByAppendingNodeWithName:nodeName]];
}

- (BOOL)isPrimitiveNode:(NSString *)nodeName {
    id<LNTXParserElementInfo> elementInfo = [self elementInfoForNodeWithName:nodeName];
    if (elementInfo) {
        return [[elementInfo elementType] isPrimitive];
    }
    
    return [self.parentContext isPrimitiveNode:[self nodeNameByAppendingNodeWithName:nodeName]];
}

- (BOOL)isContainerNode:(NSString *)nodeName {
    id<LNTXParserElementInfo> elementInfo = [self elementInfoForNodeWithName:nodeName];
    if (elementInfo) {
        return [[elementInfo elementType] isObject];
    }
    
    return  [self.parentContext isContainerNode:[self nodeNameByAppendingNodeWithName:nodeName]];
}

#pragma mark Helper methods

- (NSString *)nodeNameByAppendingNodeWithName:(NSString *)nodeName {
    return [NSString stringWithFormat:@"%@/%@", self.nodeName, nodeName];
}

#pragma mark - Updating model data

#define LNTXPrepareInvocation(invocation, value) \
[invocation setArgument:&value atIndex:2]

- (void)setPrimitiveValue:(id)value forNodeWithName:(NSString *)nodeName {
    id<LNTXParserElementInfo> elementInfo = [self elementInfoForNodeWithName:nodeName];
    
    if (!elementInfo) {
        [self.parentContext setPrimitiveValue:value forNodeWithName:[self nodeNameByAppendingNodeWithName:nodeName]];
        return;
    }
    
    NSString *type = [elementInfo elementType].mainType;
    
    NSInvocation *invocation = [elementInfo reusableInvocation];
    
    @synchronized(invocation) {
        invocation.target = self.createdObject;
        
        id createdValue = nil;
        
        if ([type isEqualToString:LNTXParserTypeRaw]) {
            // Raw (store as is)
            createdValue = value;
            LNTXPrepareInvocation(invocation, createdValue);
        } else if ([type isEqualToString:LNTXParserTypeString]) {
            // String
            createdValue = [self stringForValue:value];
            LNTXPrepareInvocation(invocation, createdValue);
        } else if ([type isEqualToString:LNTXParserTypeInteger]) {
            // Int
            if ([value respondsToSelector:@selector(integerValue)]) {
                NSInteger primitiveValue = [value integerValue];
                LNTXPrepareInvocation(invocation, primitiveValue);
            } else if ([value respondsToSelector:@selector(intValue)]) {
                int primitiveValue = [value intValue];
                LNTXPrepareInvocation(invocation, primitiveValue);
            }
        } else if ([type isEqualToString:LNTXParserTypeBoolean]) {
            // BOOL
            if ([value respondsToSelector:@selector(boolValue)]) {
                BOOL primitiveValue = [value boolValue];
                LNTXPrepareInvocation(invocation, primitiveValue);
            }
        } else if ([type isEqualToString:LNTXParserTypeFloat]) {
            // Float
            if ([value respondsToSelector:@selector(floatValue)]) {
                float primitiveValue = [value floatValue];
                LNTXPrepareInvocation(invocation, primitiveValue);
            }
        } else if ([type isEqualToString:LNTXParserTypeTimestamp]) {
            // Timestamp to Date
            if ((createdValue = [self timestampForValue:value])) {
                [invocation setArgument:&createdValue atIndex:2];
            }
        } else if ([type isEqualToString:LNTXParserTypeChar]) {
            // Char
            if ([value isKindOfClass:[NSString class]]) {
                char primitiveValue = [value characterAtIndex:0];
                LNTXPrepareInvocation(invocation, primitiveValue);
            }
        } else if ([type hasPrefix:LNTXParserTypeDate]) {
            // Date
            createdValue = [self dateForValue:value dateDetails:[elementInfo elementType].subType.mainType];
            LNTXPrepareInvocation(invocation, createdValue);
        } else if ([type isEqualToString:LNTXParserTypeDouble]) {
            // Double
            if ([value respondsToSelector:@selector(doubleValue)])
            {
                double primitiveValue = [value doubleValue];
                LNTXPrepareInvocation(invocation, primitiveValue);
            }
        } else if ([type isEqualToString:LNTXParserTypeLong]) {
            // Long
            if ([value respondsToSelector:@selector(longLongValue)]) {
                long primitiveValue = (long)[value longLongValue];
                LNTXPrepareInvocation(invocation, primitiveValue);
            }
        } else if ([type hasPrefix:LNTXParserTypeURL]) {
            // URL
            createdValue = [[NSURL alloc] initWithString:value];
            LNTXPrepareInvocation(invocation, createdValue);
        } else {
            // Unhandled type
            return;
        }
        
        [invocation invoke];
        createdValue = nil;
    }
}

- (void)setObjectValue:(id)value forNodeWithName:(NSString *)nodeName {
    id<LNTXParserElementInfo> elementInfo = [self elementInfoForNodeWithName:nodeName];
    
    if (!elementInfo) {
        [self.parentContext setObjectValue:value forNodeWithName:nodeName];
        return;
    }
    
    // Create invocation with object as target and add value parameter
    NSInvocation *invocation = elementInfo.reusableInvocation;
    @synchronized(invocation)
    {
        invocation.target = _createdObject;
        
        // Prepare parameter
        id parameter;
        
        if ([[elementInfo elementType].mainType isEqualToString:LNTXParserTypeArray])
        {
            // Copy mutable array into array
            parameter = [value copy];
        } else {
            parameter = value;
        }
        
        [invocation setArgument:&parameter atIndex:2];
        
        [invocation invoke];
    }
}

#pragma mark - Conversion methods

- (id)stringForValue:(id)value {
    if (![value isKindOfClass:[NSString class]]) {
        if ([value respondsToSelector:@selector(stringValue)]) {
            return [value stringValue];
        }
        
        return [value description];
    }
    
    return value;
}

- (id)dateForValue:(id)value dateDetails:(NSString *)dateDetails {
    id createdValue = nil;
    
    // Divide date details into locale + format
    NSString *dateLocale = nil;
    NSString *dateFormat = nil;
    
    NSRange range = [dateDetails rangeOfString:@"##"];
    if (range.location == NSNotFound) {
        dateFormat = dateDetails;
    } else {
        dateLocale = [dateDetails substringToIndex:range.location];
        dateFormat = [dateDetails substringFromIndex:range.location + range.length];
    }
    
    static NSDateFormatter *formatter;
    static NSLocale *defaultLocale;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        defaultLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    });
    
    @synchronized (formatter) {
        formatter.dateFormat = dateFormat;
        if (dateLocale) {
            formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:dateLocale];
        } else {
            formatter.locale = defaultLocale;
        }
        
        createdValue = [formatter dateFromString:value];
    }
    
    return createdValue;
}

- (id)timestampForValue:(id)value {
    if ([value respondsToSelector:@selector(doubleValue)]) {
        NSTimeInterval timestamp = [value doubleValue];
        return [NSDate dateWithTimeIntervalSince1970:timestamp];
    }
    
    return nil;
}

#pragma mark - Helper methods

- (id<LNTXParserElementInfo>)elementInfoForNodeWithName:(NSString *)nodeName {
    return [self.configuration nodeForName:nodeName];
}

- (LNTXParserType *)typeForNodeWithName:(NSString *)nodeName {
    return [[self elementInfoForNodeWithName:nodeName] elementType];
}

@end

#pragma mark - Array context

@implementation LNTXJSONArrayContext

- (instancetype)initWithConfiguration:(id <LNTXParserConfiguration>)configuration {
    if (self = [super initWithConfiguration:configuration]) {
        self.contentType = configuration.targetType.subType;
        
        // TODO: Cache content configuration if needed
        
        if (configuration.prefix.length > 0) {
            self.acceptedNodeName = [NSString stringWithFormat:@"%@/%@", configuration.prefix, LNTXJSONArrayNodeName];
        } else {
            self.acceptedNodeName = LNTXJSONArrayNodeName;
        }
    }
    
    return self;
}

- (instancetype)initWithNodeName:(NSString *)nodeName
                   parentContext:(LNTXJSONContext *)parentContext {
    if (self = [super initWithNodeName:nodeName parentContext:parentContext]) {
        LNTXParserType *elementType = [self.parentContext typeForNodeWithName:nodeName];
        
        // TODO: Handle errors
        if ([elementType.mainType isEqualToString:LNTXParserTypeArray]) {
            // Inline array
            self.contentType = elementType.subType;
            self.acceptedNodeName = LNTXJSONArrayNodeName;
        }
    }
    
    return self;
}

- (BOOL)canHandleNodeWithName:(NSString *)nodeName {
    return [nodeName isEqualToString:self.acceptedNodeName];
}

- (BOOL)isPrimitiveNode:(NSString *)nodeName {
    return ![self isContainerNode:nodeName];
}

- (BOOL)isContainerNode:(NSString *)nodeName {
    // Config or sub array
    return [self.contentType isObject];
}

- (void)setPrimitiveValue:(NSString *)value forNodeWithName:(NSString *)nodeName {
    // Raw by default
    id createdValue = value;
    
    LNTXParserType *parserType = self.contentType;
    
    NSString *type = parserType.mainType;
    
    if ([type isEqualToString:LNTXParserTypeRaw]) {
        // Raw (store as is)
        createdValue = value;
    } else if ([type isEqualToString:LNTXParserTypeString]) {
        // String
        createdValue = [self stringForValue:value];
    } else if ([type isEqualToString:LNTXParserTypeInteger]) {
        // Int
        if ([value respondsToSelector:@selector(intValue)]) {
            createdValue = @([value intValue]);
        }
    } else if ([type isEqualToString:LNTXParserTypeBoolean]) {
        // BOOL
        if ([value respondsToSelector:@selector(boolValue)]) {
            createdValue = @([value boolValue]);
        }
    } else if ([type isEqualToString:LNTXParserTypeFloat]) {
        // Float
        if ([value respondsToSelector:@selector(floatValue)]) {
            createdValue = @([value floatValue]);
        }
    } else if ([type isEqualToString:LNTXParserTypeTimestamp]) {
        // Timestamp to Date
        createdValue = [self timestampForValue:value];
    } else if ([type isEqualToString:LNTXParserTypeChar]) {
        // Char
        if ([value isKindOfClass:[NSString class]]) {
            createdValue = @([value characterAtIndex:0]);
        }
    } else if ([type hasPrefix:LNTXParserTypeDate]) {
        // Date
        createdValue = [self dateForValue:value dateDetails:parserType.subType.mainType];
    } else if ([type isEqualToString:LNTXParserTypeDouble]) {
        // Double
        if ([value respondsToSelector:@selector(doubleValue)])
        {
            createdValue = @([value doubleValue]);
        }
    } else if ([type isEqualToString:LNTXParserTypeLong]) {
        // Long
        if ([value respondsToSelector:@selector(longLongValue)]) {
            createdValue = @([value longLongValue]);
        }
    } else if ([type isEqualToString:LNTXParserTypeURL]) {
        // URL
        createdValue = [[NSURL alloc] initWithString:value];
    }
    
    if (createdValue) {
        [self setObjectValue:createdValue forNodeWithName:nodeName];
    }
}

- (void)setObjectValue:(id)value forNodeWithName:(NSString *)nodeName {
    [self.createdObject addObject:value];
}

- (LNTXJSONContext *)contextForObjectNodeWithName:(NSString *)nodeName {
    if ([self.contentType.mainType isEqualToString:LNTXParserTypeArray]) {
        // Array
        return [[LNTXJSONArrayContext alloc] initWithNodeName:LNTXJSONArrayNodeName parentContext:self];
    }
    
    // Regular object
    return [LNTXJSONContext contextWithConfiguration:[LNTXParserConfiguration configurationFromPlistFileWithName:self.contentType.mainType
                                                                                                        inBundle:self.bundle
                                                                                                           error:nil]];
}

- (LNTXParserType *)typeForNodeWithName:(NSString *)nodeName {
    return self.contentType;
}

@end

#pragma mark - Skipped context

@implementation LNTXJSONSkippedContext

#pragma mark NSObject

- (NSString *)description
{
    return [self.parentContext description];
}

#pragma mark Overriden methods

- (LNTXJSONContext *)contextForObjectNodeWithName:(NSString *)nodeName {
    return [self.parentContext contextForObjectNodeWithName:[self nodeNameByAppendingNodeWithName:nodeName]];
}

- (BOOL)canHandleNodeWithName:(NSString *)nodeName {
    // Forward to parent
    return [self.parentContext canHandleNodeWithName:[self nodeNameByAppendingNodeWithName:nodeName]];
}

- (BOOL)isPrimitiveNode:(NSString *)nodeName {
    // Forward to parent
    return [self.parentContext isPrimitiveNode:[self nodeNameByAppendingNodeWithName:nodeName]];
}

- (BOOL)isContainerNode:(NSString *)nodeName {
    // Forward to parent
    return [self.parentContext isContainerNode:[self nodeNameByAppendingNodeWithName:nodeName]];
}

- (void)setPrimitiveValue:(id)value forNodeWithName:(NSString *)nodeName {
    // Forward to parent
    [self.parentContext setPrimitiveValue:value
                          forNodeWithName:[self nodeNameByAppendingNodeWithName:nodeName]];
}

- (void)setObjectValue:(id)value forNodeWithName:(NSString *)nodeName {
    // Forward to parent
    [self.parentContext setObjectValue:value
                       forNodeWithName:[self nodeNameByAppendingNodeWithName:nodeName]];
}

@end
